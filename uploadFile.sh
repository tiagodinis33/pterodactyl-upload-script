export UPLOAD_URL="`curl "https://$1/api/client/servers/$2/files/upload" -H "Authorization: Bearer $3" -H "Accept: Application/vnd.pterodactyl.v1+json" -H "Content-Type: application/json" -s | jq -r .attributes.url`&directory=$4"
curl $UPLOAD_URL -F "files=@$5" -H "Content-Type: multipart/form-data" -H "Accept: application/json" --location > /dev/null
