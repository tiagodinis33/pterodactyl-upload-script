# pterodactyl-upload-script

this needs jq to process jsons, so you need to install with:  
    `<pkg-manager> install jq`  
Usage:  
    `./uploadFile.sh $PANEL_HOST $SERVER_ID $TOKEN $REMOTE_DIRECTORY $FILE`
